import { Place } from '../models';

export const placeList = {
	DINOVILLE: {
		placeId: 1,
		name: 'Dinoville'
	}
} as { [name: string]: Place };

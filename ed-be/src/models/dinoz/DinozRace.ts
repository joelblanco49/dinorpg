export interface DinozRace {
	raceId: number;
	name: string;
	nbrFireCase: number;
	nbrWoodCase: number;
	nbrWaterCase: number;
	nbrLightCase: number;
	nbrAirCase: number;
	price: number;
	swfLetter: string;
	skillId?: number;
}

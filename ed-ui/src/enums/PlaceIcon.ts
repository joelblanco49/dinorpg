export enum PlaceIcon {
	CHURCH = 'church',
	CASTLE = 'castle',
	HOUSE = 'house',
	CAVERN = 'cavern',
	FOUNT = 'fount'
}

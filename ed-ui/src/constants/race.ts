export const raceList = {
	1: 'winks',
	2: 'sirain',
	3: 'castivore',
	4: 'nuagoz',
	5: 'gorilloz',
	6: 'wanwan',
	7: 'pigmou',
	8: 'planaille',
	9: 'moueffe',
	10: 'rocky',
	11: 'hippoclamp',
	12: 'pteroz',
	14: 'quetzu'
};
